
FILESEXTRAPATHS_append := "${THISDIR}/netdata:"
# extra application groups
SRC_URI += "file://apps_bts_groups.conf"

do_install_append() {
	cat "${WORKDIR}/apps_bts_groups.conf" >> ${D}${sysconfdir}/netdata/apps_groups.conf
}
