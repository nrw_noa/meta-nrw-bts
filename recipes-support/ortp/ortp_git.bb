require ${PN}.inc
DEPENDS = "glib-2.0 openssl"

S = "${WORKDIR}/git"

NRW_NOA_MIRROR ??= "git@gitlab.com/nrw_noa"

inherit gitver-pkg gitver-repo

REPODIR   = "${THISDIR}"
REPOFILE  = "ortp_git.bb"
PR       := "${INC_PR}.${REPOGITFN}"

PV   = "git${SRCPV}" 
PKGV = "${PKGGITV}"
    
DEV_BRANCH  = "${@ 'nrw/next' if d.getVar('NRW_BTS_DEVEL', False) == "next" else 'nrw/master'}"
DEV_SRCREV  = "${AUTOREV}"
DEV_SRCURI := "git://${NRW_NOA_MIRROR}/ortp.git;protocol=ssh;branch=${DEV_BRANCH}"

REL_BRANCH  = "nrw/master"
REL_SRCREV  = "ba87d62d270bbb303cd1efc6d6296f8d8be99ba8"
REL_SRCURI := "git://${NRW_NOA_MIRROR}/ortp.git;protocol=ssh;branch=${REL_BRANCH}"

BRANCH  = "${@ '${DEV_BRANCH}' if d.getVar('NRW_BTS_DEVEL', False) else '${REL_BRANCH}'}"
SRCREV  = "${@ '${DEV_SRCREV}' if d.getVar('NRW_BTS_DEVEL', False) else '${REL_SRCREV}'}"
SRC_URI = "${@ '${DEV_SRCURI}' if d.getVar('NRW_BTS_DEVEL', False) else '${REL_SRCURI}'}"

addtask showversion after do_compile before do_install
do_showversion() {
    bbplain "${PN}: ${PKGGITV} => ${BRANCH}:${PKGGITH}"
}

do_compile_append() {
        sed -i -e s:${STAGING_DIR_TARGET}::g \
               -e s:/${TARGET_SYS}::g \
                  ortp.pc
}
