DESCRIPTION = "An LGPL implementation of RTP - RFC3550"
LICENSE = "LGPLv2.1+"

inherit gitver-repo

REPODIR   = "${THISDIR}"
REPOFILE  = "ortp.inc"
INC_PR   := "r${REPOGITFN}"

inherit autotools pkgconfig

LIC_FILES_CHKSUM = "file://COPYING;md5=7fbc338309ac38fefcd64b04bb903e34"

