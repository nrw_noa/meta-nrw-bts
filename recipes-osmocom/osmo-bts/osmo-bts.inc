DESCRITOPN = "Osmocom BTS-Side code (Abis, scheduling)"
DEPENDS = "libosmocore libosmo-abis"
RDEPENDS_${PN} += "coreutils lc15-firmware"
HOMEPAGE = "http://openbts.org/"
LICENSE = "AGPLv3"

LIC_FILES_CHKSUM = "file://COPYING;md5=73f1eb20517c55bf9493b7dd6e480788"

inherit gitver-repo

REPODIR   = "${THISDIR}"
REPOFILE  = "osmo-bts.inc"
INC_PR   := "r${REPOGITFN}"

inherit autotools pkgconfig systemd

EXTRA_OECONF += "--enable-litecell15"


