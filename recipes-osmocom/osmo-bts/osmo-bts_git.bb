require ${PN}.inc

DEPENDS = "libosmocore libosmo-abis lc15-firmware systemd gpsd"
RDEPENDS_${PN} += "systemd gpsd"

NRW_NOA_MIRROR ??= "git@gitlab.com/nrw_noa"

S = "${WORKDIR}/git"

inherit gitver-pkg gitver-repo

REPODIR   = "${THISDIR}"
REPOFILE  = "osmo-bts_git.bb"
PR       := "${INC_PR}.${REPOGITFN}"

REPODIR   = "${THISDIR}/files"
REPOFILE  = ""
PR       := "${PR}.${REPOGITFN}"

PV   = "git${SRCPV}" 
PKGV = "${PKGGITV}"
    
DEV_BRANCH  = "${@ 'nrw/next' if d.getVar('NRW_BTS_DEVEL', False) == "next" else 'nrw/master'}"
DEV_SRCREV  = "${AUTOREV}"
DEV_SRCURI := "git://${NRW_NOA_MIRROR}/osmo-bts.git;protocol=ssh;branch=${DEV_BRANCH};name=osmobts;destsuffix=git"

REL_BRANCH  = "nrw/master"
REL_SRCREV 	= "a4fa78ad7fff8578589d68c13c40a4b69322a94c"
REL_SRCURI	:= "git://${NRW_NOA_MIRROR}/osmo-bts.git;protocol=ssh;branch=${REL_BRANCH};name=osmobts;destsuffix=git"

BRANCH         := "${@ '${DEV_BRANCH}' if d.getVar('NRW_BTS_DEVEL', False) else '${REL_BRANCH}'}"
SRCREV		= "${@ '${DEV_SRCREV}' if d.getVar('NRW_BTS_DEVEL', False) else '${REL_SRCREV}'}"
SRC_URI		= "${@ '${DEV_SRCURI}' if d.getVar('NRW_BTS_DEVEL', False) else '${REL_SRCURI}'}"


addtask showversion after do_compile before do_install
do_showversion() {
	bbplain "${PN}: ${PKGGITV} => ${BRANCH}:${PKGGITH}"
}

do_configure_prepend() {
	ln -sf ../openbsc openbsc || true
}

do_install_append() {
	install -d ${D}${sysconfdir}/osmocom
        install -d ${D}${sysconfdir}/defaultconfig
        install -d ${D}${sysconfdir}/defaultconfig/config
        install -d ${D}${sysconfdir}/defaultconfig/config/osmocom
        install -m 0660 ${THISDIR}/files/osmocom/* ${D}${sysconfdir}/defaultconfig/config/osmocom

	# disable LED controlled by the BTS
	sed -i '/bts 0/a \ led-control-mode external' ${D}${sysconfdir}/defaultconfig/config/osmocom/osmo-bts.cfg

	# install systemd
	install -d ${D}${systemd_unitdir}/system/multi-user.target.wants/
	install -m 0644 ${THISDIR}/files/contrib/${PN}.service ${D}${systemd_unitdir}/system/
	install -m 0644 ${THISDIR}/files/contrib/${PN}-mgr.service ${D}${systemd_unitdir}/system/

	# enable systemd on sysinit
	ln -sf ../${PN}.service ${D}${systemd_unitdir}/system/multi-user.target.wants/
	ln -sf ../${PN}-mgr.service ${D}${systemd_unitdir}/system/multi-user.target.wants/

	ln -sf /mnt/rom/user/config/osmocom/osmo-bts.cfg ${D}${sysconfdir}/osmocom/osmo-bts.cfg
	ln -sf /mnt/rom/user/config/osmocom/lc15bts-mgr.cfg ${D}${sysconfdir}/osmocom/lc15bts-mgr.cfg
}

CONFFILES_${PN} = "${sysconfdir}/osmocom/osmo-bts.cfg"
CONFFILES_${PN} += "${sysconfdir}/osmocom/lc15bts-mgr.cfg"

SYSTEMD_PACKAGES = "${PN}"
SYSTEMD_SERVICE_${PN} = "${PN}.service"
SYSTEMD_SERVICE_${PN} += "${PN}-mgr.service"

FILES_${PN} += "${systemd_unitdir}"
