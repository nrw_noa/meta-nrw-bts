require ${PN}.inc

DEPENDS = "libosmocore lc15-firmware osmo-bts"

S = "${WORKDIR}/git"

NRW_NOA_MIRROR ??= "git@gitlab.com/nrw_noa"

inherit gitver-pkg gitver-repo

REPODIR   = "${THISDIR}"
REPOFILE  = "osmo-pcu_git.bb"
PR       := "${INC_PR}.${REPOGITFN}"

REPODIR   = "${THISDIR}/files"
REPOFILE  = ""
PR       := "${PR}.${REPOGITFN}"

PV   = "git${SRCPV}" 
PKGV = "${PKGGITV}"
    
DEV_BRANCH  = "${@ 'nrw/next' if d.getVar('NRW_BTS_DEVEL', False) == "next" else 'nrw/master'}"
DEV_SRCREV  = "${AUTOREV}"
DEV_SRCURI := "git://${NRW_NOA_MIRROR}/osmo-pcu.git;protocol=ssh;branch=${DEV_BRANCH}"

REL_BRANCH  = "nrw/master"
REL_SRCREV  = "f669d21b675ccee09d44c6c5c9cd32fb60b2a7a5"
REL_SRCURI := "git://${NRW_NOA_MIRROR}/osmo-pcu.git;protocol=ssh;branch=${REL_BRANCH}"

BRANCH  = "${@ '${DEV_BRANCH}' if d.getVar('NRW_BTS_DEVEL', False) else '${REL_BRANCH}'}"
SRCREV  = "${@ '${DEV_SRCREV}' if d.getVar('NRW_BTS_DEVEL', False) else '${REL_SRCREV}'}"
SRC_URI = "${@ '${DEV_SRCURI}' if d.getVar('NRW_BTS_DEVEL', False) else '${REL_SRCURI}'}"

addtask showversion after do_compile before do_install
do_showversion() {
    bbplain "${PN}: ${PKGGITV} => ${BRANCH}:${PKGGITH}"
}

do_install_append() {
        install -d ${D}${sysconfdir}/osmocom
        install -d ${D}${sysconfdir}/defaultconfig
        install -d ${D}${sysconfdir}/defaultconfig/config
        install -d ${D}${sysconfdir}/defaultconfig/config/osmocom
        install -m 0660 ${THISDIR}/files/osmocom/* ${D}${sysconfdir}/defaultconfig/config/osmocom

	# install systemd
        install -d ${D}${systemd_unitdir}/system/multi-user.target.wants/
        install -m 0644 ${THISDIR}/files/contrib/osmo-pcu.service ${D}${systemd_unitdir}/system/

        # enable systemd on sysinit
        ln -sf ../osmo-pcu.service ${D}${systemd_unitdir}/system/multi-user.target.wants/

	ln -sf /mnt/rom/user/config/osmocom/osmo-pcu.cfg ${D}${sysconfdir}/osmocom/osmo-pcu.cfg
}

CONFFILES_${PN} = "${sysconfdir}/osmocom/osmo-pcu.cfg"

FILES_${PN} += "${systemd_unitdir}"

SYSTEMD_PACKAGES = "${PN}"
SYSTEMD_SERVICE_${PN} = "${PN}.service"
