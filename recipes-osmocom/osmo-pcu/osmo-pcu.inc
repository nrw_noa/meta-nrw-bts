DESCRITOPN = "Osmocom PCU for NuRAN Wireless Litecell 1.5 BTS"
DEPENDS = "libosmocore"
RDEPENDS_${PN} = "lc15-firmware osmo-bts"
HOMEPAGE = "git.osmocom.org/osmo-pcu"
LICENSE = "GPLv2"

LIC_FILES_CHKSUM = "file://COPYING;md5=b234ee4d69f5fce4486a80fdaf4a4263"

inherit gitver-repo

REPODIR   = "${THISDIR}"
REPOFILE  = "osmo-pcu.inc"
INC_PR   := "r${REPOGITFN}"

inherit autotools pkgconfig systemd

EXTRA_OECONF="--enable-lc15bts-phy --with-litecell15=src/osmo-bts-litecell15"


