DESCRIPTION = "An utility library for Open Source Mobile Communications"
HOMEPAGE = "http://openbsc.gnumonks.org"
LICENSE = "GPLv2+"

inherit gitver-repo

REPODIR   = "${THISDIR}"
REPOFILE  = "libosmocore.inc"
INC_PR   := "r${REPOGITFN}"

LIC_FILES_CHKSUM = "file://COPYING;md5=751419260aa954499f7abaabaa882bbe"

inherit autotools pkgconfig

