# Based on recipe from meta-telephony
require ${PN}.inc

DEPENDS = "libosmocore ortp"

S = "${WORKDIR}/git"

NRW_NOA_MIRROR ??= "git@gitlab.com/nrw_noa"

inherit gitver-pkg gitver-repo

REPODIR   = "${THISDIR}"
REPOFILE  = "libosmo-abis_git.bb"
PR       := "${INC_PR}.${REPOGITFN}"

REPODIR   = "${THISDIR}/files"
REPOFILE  = ""
PR       := "${PR}.${REPOGITFN}"

PV   = "git${SRCPV}" 
PKGV = "${PKGGITV}"
    
DEV_BRANCH  = "${@ 'nrw/next' if d.getVar('NRW_BTS_DEVEL', False) == "next" else 'nrw/master'}"
DEV_SRCREV  = "${AUTOREV}"
DEV_SRCURI := "git://${NRW_NOA_MIRROR}/libosmo-abis.git;protocol=ssh;branch=${DEV_BRANCH}"

REL_BRANCH  = "nrw/master"
REL_SRCREV  = "c46d4ed728b90d41fbbd034e63607213ff1d3b62"
REL_SRCURI := "git://${NRW_NOA_MIRROR}/libosmo-abis.git;protocol=ssh;branch=${REL_BRANCH}"

BRANCH  = "${@ '${DEV_BRANCH}' if d.getVar('NRW_BTS_DEVEL', False) else '${REL_BRANCH}'}"
SRCREV  = "${@ '${DEV_SRCREV}' if d.getVar('NRW_BTS_DEVEL', False) else '${REL_SRCREV}'}"
SRC_URI = "${@ '${DEV_SRCURI}' if d.getVar('NRW_BTS_DEVEL', False) else '${REL_SRCURI}'}"

addtask showversion after do_compile before do_install
do_showversion() {
    bbplain "${PN}: ${PKGGITV} => ${BRANCH}:${PKGGITH}"
}
