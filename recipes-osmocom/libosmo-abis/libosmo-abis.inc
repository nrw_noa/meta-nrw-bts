DESCRITOPN = "An utility library for Open Source Mobile Communications"
DEPENDS = "libosmocore ortp (>= 0.22.0)"
HOMEPAGE = "http://openbsc.gnumonks.org"
LICENSE = "AGPLv3"

inherit gitver-repo

REPODIR   = "${THISDIR}"
REPOFILE  = "libosmo-abis.inc"
INC_PR   := "r${REPOGITFN}"

LIC_FILES_CHKSUM = "file://COPYING;md5=73f1eb20517c55bf9493b7dd6e480788"

inherit autotools pkgconfig

ALLOW_EMPTY_libosmo-abis = "1"
