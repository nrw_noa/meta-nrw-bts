SUMMARY = "Litecell15 Linux image with Oscomcom software installed"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LICENSE;md5=3f40d7994397109285ec7b81fdeb3b58"

inherit gitver-repo

# Depend on meta-nrw-bsp
require ../../meta-nrw-bsp/recipes-core/images/litecell15-image.bb

REPODIR   = "${THISDIR}"
REPOFILE  = ""
PR       := "r${REPOGITFN}"
PV       := "${REPOGITT}-${REPOGITN}"

IMAGE_NAME = "${PN}-${PV}-${PR}-${DATETIME}"
BTS_LAYER_VERSION := "${PV}-${PR}"


install_lc15_manifest_append() {
    printf "           BTS : %s\n" "${BTS_LAYER_VERSION}" >> ${IMAGE_ROOTFS}/etc/litecell15.manifest
}

IMAGE_INSTALL+="packagegroup-bts \
		"
inherit core-image
