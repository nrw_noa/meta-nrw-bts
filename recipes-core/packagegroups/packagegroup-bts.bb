DESCRIPTION = "Osmo-BTS default Package Groups"
LICENSE = "MIT"

inherit packagegroup

PACKAGES = "\
         packagegroup-bts \
         "

RDEPENDS_packagegroup-bts = "\
	osmo-bts \
	osmo-pcu \
	"
